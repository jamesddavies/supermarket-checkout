const { Items, Offers } = require('./data');

class Transaction {
    constructor(skuArray) {
        this.skuArray = skuArray;
    }

    get itemList() {
        return this.createItemList();
    }

    createItemList() {
        return this.skuArray.map(sku => ({ name: [sku], value: Items[sku] ? Items[sku] : '0 - item not found' }));
    }

    get subtotal() {
        return this.calculateSubtotal();
    }

    calculateSubtotal() {
        return this.skuArray.reduce((acc, sku) => acc += Items[sku] ? Items[sku] : 0, 0);
    }

    get total() {
        return this.calculateTotal();
    }

    calculateTotal() {
        return this.applyOffers(this.skuArray);
    }

    applyOffers() {
        let total = 0;
        let itemMap = this.createItemMap();

        while (this.availableOffers(itemMap).length) {
            this.availableOffers(itemMap).forEach(offer => {
                offer.items.forEach(offerItem => {
                    itemMap[offerItem.item] = itemMap[offerItem.item] - offerItem.amount;
                });
                total += offer.price;
            });
        }

        for (let itemName in itemMap) {
            total += (Items[itemName] * itemMap[itemName]);
        }

        return total;
    }

    availableOffers(itemMap) {
        return Offers.filter(offer => (           
            offer.items.reduce((acc, offerItem) => {
                acc = itemMap[offerItem.item] >= offerItem.amount
                return acc;
            }, false)
        ));
    }

    createItemMap() {
        const filteredSkuArray = this.skuArray.filter(sku => !!Items[sku]);
        return filteredSkuArray.reduce((acc, sku) => {
            acc[sku] ? acc[sku]++ : acc[sku] = 1;
            return acc;
        }, {});
    }
}

module.exports = Transaction;