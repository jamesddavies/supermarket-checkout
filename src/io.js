const chalk = require('chalk');
const readline = require('readline');

class IO {
    constructor(currencySymbol) {
        this.currencySymbol = currencySymbol;
        this.inputRequestMessage = 'Please enter a string of SKUs (e.g. AABA): ';
        this.invalidInputMessage = 'Input invalid. Please try again.';
        this.restartOrExitMessage = '\nPlease enter R to scan more items or any other key to exit. \n';
        this.rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
    }

    requestInput(callback) {
        this.rl.question(this.inputRequestMessage, (answer) => {
            this.handleInput(answer, callback);
        });
    }

    handleInput(input, callback) {
        if (this.inputIsValid(input)) {
            callback(this.processInput(input));
            this.restartOrExit(callback);
        } else {
            console.log(chalk.red(this.invalidInputMessage));
            this.requestInput(callback);
        }
    }

    inputIsValid(input) {
        // NOTE: Input should either be an empty string or only contain letters.
        return input === "" ? true : /^[A-Za-z]+$/.test(input);
    }

    processInput(input) {
        return input.toUpperCase().split("").sort();
    }

    restartOrExit(callback) {
        this.rl.question(this.restartOrExitMessage, (answer) => {
            if (answer.toLowerCase() === 'r') {
                this.requestInput(callback);
            } else {
                process.exit(0);
            }
        })
    }

    printResults(itemList, subtotal, total) {
        console.log(itemList.map(item => `${item.name}: ${!isNaN(item.value) ? this.currencySymbol + item.value : item.value}\n`).join(''));
        console.log(`Subtotal: ${this.currencySymbol}${subtotal}\n`);
        console.log(`Total: ${chalk.green(this.currencySymbol + total)}`);
    }
}

module.exports = IO;