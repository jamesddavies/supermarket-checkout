const IO = require('./io');
const Transaction = require('./transaction');

class Checkout {
    constructor() {
        this.currencySymbol = '£';
        this.IO = new IO(this.currencySymbol);
    }

    start() {
        this.IO.requestInput(this.checkoutItems.bind(this));
    }

    checkoutItems(skuArray) {
        const transaction = new Transaction(skuArray);
        this.IO.printResults(
            transaction.itemList,
            transaction.subtotal,
            transaction.total
        );
    }
}

module.exports = Checkout;