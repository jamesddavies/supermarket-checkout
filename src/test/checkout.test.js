const assert = require('assert');
const Checkout = require('../checkout');
const Transaction = require('../transaction');

function baseTest(input, expectedOutput) {
    const processedInput = this.checkout.IO.processInput(input);
    const transaction = new Transaction(processedInput);
    assert.equal(transaction.total, expectedOutput);
}

describe('Checkout Test Suite', () => {

    before('Initialising a new checkout', () => {
        this.checkout = new Checkout();
    })

    it('should return 0 for an empty string', () => {
        baseTest.call(this, '', 0);
    });

    it('should equal 50 when A is entered', () => {
        baseTest.call(this, 'A', 50);
    });

    it('should equal 80 when AB is entered', () => {
        baseTest.call(this, 'AB', 80);
    });

    it('should equal 115 when CDBA is entered', () => {
        baseTest.call(this, 'CDBA', 115);
    });

    it('should equal 100 when AA is entered', () => {
        baseTest.call(this, 'AA', 100);
    });

    it('should equal 130 when AAA is entered', () => {
        baseTest.call(this, 'AAA', 130);
    });

    it('should equal 175 when AAABB is entered', () => {
        baseTest.call(this, 'AAABB', 175);
    });

    it('should equal 260 when AAAAAA is entered', () => {
        baseTest.call(this, 'AAAAAA', 260);
    })

    it('should equal 350 when AABBAABBAA is entered', () => {
        baseTest.call(this, 'AABBAABBAA', 350);
    })

});