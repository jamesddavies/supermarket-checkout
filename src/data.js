const Items = {
    A: 50,
    B: 30,
    C: 20,
    D: 15
};

const Offers = [
    {
        items: [ 
            {
                item: 'A',
                amount: 3
            }
        ], 
        price: 130
    },
    {
        items: [
            {
                item: 'B',
                amount: 2
            }
        ],
        price: 45
    }
];

module.exports = {
    Items,
    Offers
}