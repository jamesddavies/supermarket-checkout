## Running the project:

Clone the repo, then:
```
npm install
npm start
```

## Running tests:

```
npm test
```